## Building and Running the Application
The following commands should be executed from the root directory of the repository:

`./gradlew clean build` - Run this command to build the application, run all of the tests, and produce build artifacts (both a jar and a docker image) that can be deployed.

`./gradlew clean run` - Run this command to build the application and run it as a JVM application in the terminal.

The application can also be run from the jar file produced during the build. To do so, run the following after the gradle build command above:
`java -jar build/libs/ratecalculator.jar`
For convenience, a pre-built jar file is also located in the `artifacts` directory that you can run without building the application.

Once the app is running, the following endpoints will be available: `localhost:8080/rate` and `localhost:8080/metrics`. Details about the endpoints can be found in the API section below.

### Replacing the JSON rates file
The does not support updating the JSON rates file while the app is running or supplying a file through command line arguments on startup. It will always load the `rates.json` file located in `src/main/resources`. You can replace this file to use a new set of rates. Be sure to re-build the app to ensure that the new json file is loaded into the jar/docker image.

## Assumptions
The following are some assumptions made during development of the app:
* The supplied JSON file with rates is in expected format. There is no error checking/handling performed when parsing the file.
* All times/days listed in the JSON rates file and in queries are expected to be UTC.
* The API contract is followed (i.e. all query parameters are supplied and are in expected format). There is no parameter validation done when the HTTP request is received.

## Design Notes
Overall, I tried to maintain the Single Responsibility Principle and keep the application modular. Almost all of dependencies for each class in the app are injected through the class constructor to making mocking/unit testing very simple. The following is a list of the major classes in the application, along with a brief description of their responsiblity:
* `JsonParser` - This class parses the JSON rates file and populates a new instance of `RateRepository` with the parsed data. 
* `RateRepository` - This class stores all of the rate information. This is the "database" layer of the application. It's designed so that it could be easily swapped out for a real database in the future.
* `RatesService` - This class contains all of the business logic for the rates endpoint. It contains a reference to the `RateRepository` so it can construct queries for rate data and generate an appropriate response. 
* `RatesServlet` - This is the servlet handler for the `/rate` endpoint. It captures each parameter from the HTTP request and passes them along to the `RatesService`. It also formats the response in either XML or JSON based on the request header. 
* `AppBuilder` - This class is used to instantiate all of the app dependencies in the correct order. It helps in de-cluttering the main function, eases integration test setup, and could be expanded on later for further orchestrating the startup/shutdown of the different classes if, for instance, they were running in separate threads.

## Deployment
The application is packaged as a fat-jar so it would be pretty easy to deploy it anywhere, assuming the deployment environment has the JVM installed. As part of the build process, a docker image is also created to ensure consistent deployments across environments. If you'd like to run the docker image locally, the following command can be used after the gradle build command has been run:
`docker run -p 8080:8080 ratecalculator:latest`

## Metrics
The Dropwizard Metrics library is used to collect metrics on the `/rate` endpoint. The metrics can be viewed by querying the `/metrics` endpoint. It returns a JSON response (XML not supported) containing a count of the number of requests and statistics about the latency of the requests.

## Rate application API
* `/rate` - Returns a price for the specified time range
  * Headers
    * `Accept:application/xml` - if XML is specified in the header, then the response content type will be XML. It will be JSON otherwise.
  * Parameters
    * `start` - timestamp in ISO format (e.g. 2015-04-14T11:07:36.639Z)
    * `stop` - timestamp in ISO format
  * Sample request - `curl -i -H "Accept:application/xml" "localhost:8080/rate?start=2015-04-14T11:07:36.639Z&stop=2015-04-14T16:37:37.639Z"`
  * Response format
    * XML - ```<?xml version="1.0" encoding="UTF-8"?>
               <rate>
                  <start>[START_TIME]</start>
                  <stop>[STOP_TIME]</stop>
                  <price>[PRICE]</price>
               </rate>```
    * JSON - ```{
                   "start": [START_TIME],
                   "stop": [STOP_TIME],
                   "price": [PRICE]
                }```



