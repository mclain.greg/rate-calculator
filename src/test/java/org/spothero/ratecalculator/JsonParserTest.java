package org.spothero.ratecalculator;

import static org.junit.Assert.assertEquals;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.DayOfWeek;
import java.util.List;
import org.junit.Test;
import org.spothero.ratecalculator.rates.JsonParser;
import org.spothero.ratecalculator.rates.RateRange;
import org.spothero.ratecalculator.rates.RateRepository;

public class JsonParserTest {

    JsonParser parser = new JsonParser();

    @Test
    public void loadFromReader_SingleDay() {
        String str = "    {\n" +
                "      \"rates\": [\n" +
                "        {\n" +
                "          \"days\": \"mon\",\n" +
                "          \"times\": \"0600-1800\",\n" +
                "          \"price\": 1500\n" +
                "        }\n" +
                "      ]\n" +
                "    }";
        InputStream is = new ByteArrayInputStream(str.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        RateRepository rateRepository = parser.loadFromReader(br);
        List<RateRange> mondayRates = rateRepository.getRatesForDay(DayOfWeek.MONDAY);
        assertEquals(1, mondayRates.size());
        RateRange rate = mondayRates.get(0);
        assertEquals(600, rate.getStartTime());
        assertEquals(1800, rate.getEndTime());
        assertEquals(1500, rate.getPrice());
    }

    @Test
    public void loadFromReader_MultipleDays() {
        String str = "    {\n" +
                "      \"rates\": [\n" +
                "        {\n" +
                "          \"days\": \"tues,wed\",\n" +
                "          \"times\": \"0600-1800\",\n" +
                "          \"price\": 1500\n" +
                "        }\n" +
                "      ]\n" +
                "    }";
        InputStream is = new ByteArrayInputStream(str.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        RateRepository rateRepository = parser.loadFromReader(br);
        List<RateRange> tuesdayRates = rateRepository.getRatesForDay(DayOfWeek.TUESDAY);
        assertEquals(1, tuesdayRates.size());
        RateRange rate = tuesdayRates.get(0);
        assertEquals(600, rate.getStartTime());
        assertEquals(1800, rate.getEndTime());
        assertEquals(1500, rate.getPrice());

        List<RateRange> wednesdayRates = rateRepository.getRatesForDay(DayOfWeek.WEDNESDAY);
        assertEquals(1, wednesdayRates.size());
        rate = wednesdayRates.get(0);
        assertEquals(600, rate.getStartTime());
        assertEquals(1800, rate.getEndTime());
        assertEquals(1500, rate.getPrice());
    }

    @Test
    public void loadFromReader_MultipleRanges() {
        String str = "    {\n" +
                "      \"rates\": [\n" +
                "        {\n" +
                "          \"days\": \"thurs\",\n" +
                "          \"times\": \"0600-1800\",\n" +
                "          \"price\": 1500\n" +
                "        },\n" +
                "        {\n" +
                "          \"days\": \"thurs\",\n" +
                "          \"times\": \"0300-0400\",\n" +
                "          \"price\": 2000\n" +
                "        }\n" +
                "      ]\n" +
                "    }";
        InputStream is = new ByteArrayInputStream(str.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        RateRepository rateRepository = parser.loadFromReader(br);
        List<RateRange> thursdayRates = rateRepository.getRatesForDay(DayOfWeek.THURSDAY);
        assertEquals(2, thursdayRates.size());
        RateRange rate = thursdayRates.get(0);
        assertEquals(600, rate.getStartTime());
        assertEquals(1800, rate.getEndTime());
        assertEquals(1500, rate.getPrice());
        rate = thursdayRates.get(1);
        assertEquals(300, rate.getStartTime());
        assertEquals(400, rate.getEndTime());
        assertEquals(2000, rate.getPrice());
    }
}