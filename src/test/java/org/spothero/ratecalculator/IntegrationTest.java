package org.spothero.ratecalculator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class IntegrationTest {

    AppBuilder appBuilder;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        appBuilder = new AppBuilder();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getRate_JsonResponse() throws IOException {
        appBuilder.buildContext("integration_test_rates.json");
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(printWriter);
        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("start", new String[]{"2017-05-15T11:00:45.901Z"});
        parameters.put("stop", new String[]{"2017-05-15T13:07:21.339Z"});
        when(request.getHeader("accept")).thenReturn("application/json");
        when(request.getParameterMap()).thenReturn(parameters);

        appBuilder.getRatesServlet().doGet(request, response);

        assertEquals("{\n" +
                "  \"start\": \"2017-05-15T11:00:45.901Z\",\n" +
                "  \"stop\": \"2017-05-15T13:07:21.339Z\",\n" +
                "  \"price\": \"200\"\n" +
                "}", stringWriter.toString());
    }

    @Test
    public void getRate_JsonResponseUnavailable() throws IOException {
        appBuilder.buildContext("integration_test_rates.json");
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(printWriter);
        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("start", new String[]{"2017-05-15T01:00:45.901Z"});
        parameters.put("stop", new String[]{"2017-05-15T23:07:21.339Z"});
        when(request.getHeader("accept")).thenReturn("application/json");
        when(request.getParameterMap()).thenReturn(parameters);

        appBuilder.getRatesServlet().doGet(request, response);

        assertEquals("{\n" +
                "  \"start\": \"2017-05-15T01:00:45.901Z\",\n" +
                "  \"stop\": \"2017-05-15T23:07:21.339Z\",\n" +
                "  \"price\": \"unavailable\"\n" +
                "}", stringWriter.toString());
    }

    @Test
    public void getRate_XmlResponse() throws IOException {
        appBuilder.buildContext("integration_test_rates.json");
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getWriter()).thenReturn(printWriter);
        HttpServletRequest request = mock(HttpServletRequest.class);
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("start", new String[]{"2017-05-15T11:00:45.901Z"});
        parameters.put("stop", new String[]{"2017-05-15T13:07:21.339Z"});
        when(request.getHeader("accept")).thenReturn("application/xml");
        when(request.getParameterMap()).thenReturn(parameters);

        appBuilder.getRatesServlet().doGet(request, response);

        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<rate>" +
                "<start>2017-05-15T11:00:45.901Z</start>" +
                "<stop>2017-05-15T13:07:21.339Z</stop>" +
                "<price>200</price>" +
                "</rate>", stringWriter.toString());
    }
}
