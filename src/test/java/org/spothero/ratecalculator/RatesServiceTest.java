package org.spothero.ratecalculator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import java.time.DayOfWeek;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.spothero.ratecalculator.rates.RatesService;
import org.spothero.ratecalculator.rates.RateRepository;

public class RatesServiceTest {

    RatesService ratesService;

    @Mock
    RateRepository rateRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ratesService = new RatesService(rateRepository);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void lookupRate() {
        when(rateRepository.getRate(DayOfWeek.TUESDAY, 1107, 1637)).thenReturn(1000);
        String rate = ratesService.lookupRate("2015-04-14T11:07:36.639Z", "2015-04-14T16:37:37.639Z");
        assertEquals("1000", rate);
    }

    @Test
    public void lookupRate_Unavailable() {
        when(rateRepository.getRate(DayOfWeek.TUESDAY, 1107, 1637)).thenReturn(-1);
        String rate = ratesService.lookupRate("2015-04-14T11:07:36.639Z", "2015-04-14T16:37:37.639Z");
        assertEquals("unavailable", rate);
    }
}