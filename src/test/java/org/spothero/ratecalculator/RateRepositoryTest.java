package org.spothero.ratecalculator;

import static org.junit.Assert.*;
import java.time.DayOfWeek;
import org.junit.Test;
import org.spothero.ratecalculator.rates.RateRange;
import org.spothero.ratecalculator.rates.RateRepository;

public class RateRepositoryTest {

    RateRepository rateRepository = new RateRepository();

    @Test
    public void addAndRetrieveRate() {
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1000, 2000, 1500));
        int price = rateRepository.getRate(DayOfWeek.MONDAY, 1200, 1900);
        assertEquals(1500, price);
    }

    @Test
    public void getRate_NonExistentDay() {
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1000, 2000, 1500));
        int price = rateRepository.getRate(DayOfWeek.TUESDAY, 1200, 1900);
        assertEquals(-1, price);
    }

    @Test
    public void getRate_DayWithMultipleRanges() {
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1000, 1200, 100));
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1400, 1600, 200));
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1900, 2000, 300));
        int price = rateRepository.getRate(DayOfWeek.MONDAY, 1100, 1150);
        assertEquals(100, price);

        price = rateRepository.getRate(DayOfWeek.MONDAY, 1500, 1550);
        assertEquals(200, price);

        price = rateRepository.getRate(DayOfWeek.MONDAY, 1950, 1975);
        assertEquals(300, price);
    }

    @Test
    public void getRate_ExclusiveStartTime() {
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1000, 1200, 100));
        int price = rateRepository.getRate(DayOfWeek.MONDAY, 1000, 1100);
        assertEquals(-1, price);
    }

    @Test
    public void getRate_ExclusiveEndTime() {
        rateRepository.add(DayOfWeek.MONDAY, new RateRange(1000, 1200, 100));
        int price = rateRepository.getRate(DayOfWeek.MONDAY, 1100, 1200);
        assertEquals(-1, price);
    }
}