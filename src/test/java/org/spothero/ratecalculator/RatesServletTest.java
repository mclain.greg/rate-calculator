package org.spothero.ratecalculator;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.codahale.metrics.MetricRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.spothero.ratecalculator.rates.RatesService;
import org.spothero.ratecalculator.rates.RatesServlet;

public class RatesServletTest {

    @Mock
    RatesService ratesService;

    @Mock HttpServletResponse response;

    MetricRegistry metrics;

    @Before
    public void setUp() throws Exception {
        metrics = new MetricRegistry();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void doGet_Json() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(printWriter);
        when(ratesService.lookupRate(any(), any())).thenReturn("1000");
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("start", new String[]{"2017-05-15T11:00:45.901Z"});
        parameters.put("stop", new String[]{"2017-05-15T13:07:21.339Z"});
        when(mockRequest.getHeader("accept")).thenReturn("application/json");
        when(mockRequest.getParameterMap()).thenReturn(parameters);
        new RatesServlet(ratesService, metrics).doGet(mockRequest, response);

        assertEquals("{\n" +
                "  \"start\": \"2017-05-15T11:00:45.901Z\",\n" +
                "  \"stop\": \"2017-05-15T13:07:21.339Z\",\n" +
                "  \"price\": \"1000\"\n" +
                "}", stringWriter.toString());
    }

    @Test
    public void doGet_Xml() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(printWriter);
        when(ratesService.lookupRate(any(), any())).thenReturn("1000");
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put("start", new String[]{"2017-05-15T11:00:45.901Z"});
        parameters.put("stop", new String[]{"2017-05-15T13:07:21.339Z"});
        when(mockRequest.getHeader("accept")).thenReturn("application/xml");
        when(mockRequest.getParameterMap()).thenReturn(parameters);
        new RatesServlet(ratesService, metrics).doGet(mockRequest, response);

        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<rate>" +
                "<start>2017-05-15T11:00:45.901Z</start>" +
                "<stop>2017-05-15T13:07:21.339Z</stop>" +
                "<price>1000</price>" +
                "</rate>", stringWriter.toString());
    }
}