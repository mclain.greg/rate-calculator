package org.spothero.ratecalculator;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

public class Application {
    public static void main( String[] args ) throws Exception
    {
        Server server = new Server();

        ServerConnector http = new ServerConnector(server);
        http.setPort(8080);
        http.setIdleTimeout(30000);
        server.addConnector(http);

        AppBuilder builder = new AppBuilder();
        builder.buildContext("rates.json");
        server.setHandler(builder.getContext());
        server.start();
        server.join();
    }
}
