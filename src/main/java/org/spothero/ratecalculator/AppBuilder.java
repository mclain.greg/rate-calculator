package org.spothero.ratecalculator;

import javax.servlet.ServletContextListener;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.servlets.MetricsServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.spothero.ratecalculator.metrics.MetricsServletContextListener;
import org.spothero.ratecalculator.rates.JsonParser;
import org.spothero.ratecalculator.rates.RateRepository;
import org.spothero.ratecalculator.rates.RatesService;
import org.spothero.ratecalculator.rates.RatesServlet;

public class AppBuilder {

    private ServletContextHandler context;
    private RatesServlet ratesServlet;

    public ServletContextHandler getContext() {
        return context;
    }

    public RatesServlet getRatesServlet() {
        return ratesServlet;
    }

    public void buildContext(String ratesFile) {
        context = new ServletContextHandler();
        context.setContextPath("/");

        JsonParser parser = new JsonParser();
        RateRepository rates = parser.loadFromFile(ratesFile);

        MetricRegistry metrics = MetricsServletContextListener.METRIC_REGISTRY;
        RatesService service = new RatesService(rates);
        ratesServlet = new RatesServlet(service, metrics);
        ServletHolder ratesHolder = new ServletHolder(ratesServlet);
        context.addServlet(ratesHolder, "/rate");

        MetricsServlet metricsServlet = new MetricsServlet();
        ServletHolder metricsHolder = new ServletHolder(metricsServlet);
        context.addServlet(metricsHolder, "/metrics");
        ServletContextListener listener = new MetricsServletContextListener();
        context.addEventListener(listener);
    }
}
