package org.spothero.ratecalculator.metrics;

import javax.servlet.annotation.WebListener;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.servlets.MetricsServlet;

@WebListener
public class MetricsServletContextListener extends MetricsServlet.ContextListener {

    public static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();

    @Override
    public MetricRegistry getMetricRegistry() {
        return METRIC_REGISTRY;
    }

}