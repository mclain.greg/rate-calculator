package org.spothero.ratecalculator.rates;

import static com.codahale.metrics.MetricRegistry.name;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class RatesServlet extends HttpServlet {

    private final RatesService ratesService;

    private final Timer responseTimer;

    private static final String START = "start";
    private static final String STOP = "stop";
    private static final String XML_CONTENT_TYPE = "application/xml";
    private static final String JSON_CONTENT_TYPE = "application/json";

    public RatesServlet(RatesService ratesService, MetricRegistry metrics)
    {
        this.ratesService = ratesService;
        this.responseTimer = metrics.timer(name(RatesServlet.class, "responseTimer"));
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Timer.Context context = responseTimer.time();

        Map<String, String[]> params = request.getParameterMap();
        String price = ratesService.lookupRate(params.get(START)[0], params.get("stop")[0]);

        if (request.getHeader("accept").contains(XML_CONTENT_TYPE)) {
            response.setContentType(XML_CONTENT_TYPE);
            PrintWriter writer = response.getWriter();
            writer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.append("<rate>");
            writer.append("<"+START+">").append(params.get(START)[0]).append("</"+START+">");
            writer.append("<"+STOP+">").append(params.get(STOP)[0]).append("</"+STOP+">");
            writer.append("<price>").append(price).append("</price>");
            writer.append("</rate>");
        } else {
            response.setContentType(JSON_CONTENT_TYPE);
            JsonObject rate = new JsonObject();
            rate.addProperty(START, params.get(START)[0]);
            rate.addProperty(STOP, params.get(STOP)[0]);
            rate.addProperty("price", price);
            Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
            response.getWriter().append(gson.toJson(rate));
        }

        context.stop();
    }
}