package org.spothero.ratecalculator.rates;

public class RateRange {
    private int startTime;
    private int endTime;
    private int price;

    public RateRange(int startTime, int endTime, int price) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.price = price;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public int getPrice() {
        return price;
    }
}
