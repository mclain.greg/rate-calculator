package org.spothero.ratecalculator.rates;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class RateRepository {
    private Map<DayOfWeek, List<RateRange>> ratesMapByDay = new EnumMap<>(DayOfWeek.class);

    public void add(DayOfWeek day, RateRange range) {
        List<RateRange> rates = ratesMapByDay.getOrDefault(day, new ArrayList<>());
        rates.add(range);
        ratesMapByDay.put(day, rates);
    }

    public int getRate(DayOfWeek day, int startTime, int endTime) {
        List<RateRange> rates = getRatesForDay(day);
        for (RateRange rate : rates) {
            if (startTime > rate.getStartTime()
                    && endTime < rate.getEndTime()) {
                return rate.getPrice();
            }
        }

        return -1;
    }

    public List<RateRange> getRatesForDay(DayOfWeek day) {
        return ratesMapByDay.getOrDefault(day, new ArrayList<>());
    }
}
