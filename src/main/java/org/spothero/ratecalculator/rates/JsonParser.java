package org.spothero.ratecalculator.rates;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonParser {

    public RateRepository loadFromFile(String fileName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream(fileName);

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input, "UTF-8"));
            return loadFromReader(bufferedReader);
        } catch (UnsupportedEncodingException ex) {
            System.out.println("Caught UnsupportedEncodingException!");
        }

        return new RateRepository();
    }

    public RateRepository loadFromReader(BufferedReader reader) {
        RateRepository rateRepository = new RateRepository();
        Gson gson = new Gson();
        JsonObject json = gson.fromJson(reader, JsonObject.class);
        JsonArray ratesArray = json.getAsJsonArray("rates");
        for (JsonElement obj : ratesArray) {
            List<DayOfWeek> days = parseDays(obj.getAsJsonObject().get("days").getAsString());
            RateRange range = parseRateRange(obj.getAsJsonObject());

            for (DayOfWeek day : days) {
                rateRepository.add(day, range);
            }
        }

        return rateRepository;
    }

    private List<DayOfWeek> parseDays(String daysString) {
        List<DayOfWeek> tokens = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(daysString, ",");
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            tokens.add(mapStringToDayOfWeek(token));
        }

        return tokens;
    }

    private RateRange parseRateRange(JsonObject obj) {
        StringTokenizer tokenizer = new StringTokenizer(obj.get("times").getAsString(), "-");
        int startTime = Integer.parseInt(tokenizer.nextToken());
        int endTime = Integer.parseInt(tokenizer.nextToken());
        int price = obj.get("price").getAsInt();
        return new RateRange(startTime, endTime, price);
    }

    private DayOfWeek mapStringToDayOfWeek(String dayString) {
        DayOfWeek day = DayOfWeek.SUNDAY;
        if (dayString.equals("mon")) {
            day = DayOfWeek.MONDAY;
        } else if (dayString.equals("tues")) {
            day = DayOfWeek.TUESDAY;
        } else if (dayString.equals("wed")) {
            day = DayOfWeek.WEDNESDAY;
        } else if (dayString.equals("thurs")) {
            day = DayOfWeek.THURSDAY;
        } else if (dayString.equals("fri")) {
            day = DayOfWeek.FRIDAY;
        } else if (dayString.equals("sat")) {
            day = DayOfWeek.SATURDAY;
        }

        return day;
    }
}
