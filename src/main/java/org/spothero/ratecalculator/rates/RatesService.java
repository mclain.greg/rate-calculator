package org.spothero.ratecalculator.rates;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class RatesService {
    private RateRepository rates;

    public RatesService(RateRepository rates) {
        this.rates = rates;
    }

    public String lookupRate(String start, String stop) {
        DayOfWeek day = getDayFromIsoString(start);
        int startTime = getTimeFromIsoString(start);
        int stopTime = getTimeFromIsoString(stop);
        int rate = rates.getRate(day, startTime, stopTime);

        String returnString = "unavailable";
        if (rate > -1) {
            returnString = Integer.toString(rate);
        }

        return returnString;
    }

    private DayOfWeek getDayFromIsoString(String time) {
        Instant instant = Instant.parse(time);
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        return ldt.getDayOfWeek();
    }

    private int getTimeFromIsoString(String time) {
        Instant instant = Instant.parse(time);
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmm");
        return Integer.parseInt(formatter.format(ldt));
    }
}
