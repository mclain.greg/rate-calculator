FROM openjdk:8-alpine
ADD build/libs/ratecalculator.jar /app/ratecalculator.jar

EXPOSE 8080

CMD ["java","-jar","/app/ratecalculator.jar"]
